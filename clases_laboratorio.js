let canciones=new Array;
let cantantes=new Array;
canciones=["Procura", "Downtown", "Su veneno", "Suavemente", "Fantasias", "Imitadora", "Otro trago", "La canción", "Dulce pecado", "Murió el amor", "Aventurero"];
cantantes=["Chichi Peralta", "Annita y J balvin", "Aventura", "Elvis Crespo", "Rauw Alejandro Farruko", "Romeo Santos", "Sech", "J balvin y Bad bunny", "Jessi Uribe", "Paola Jara", "Yeison Jiménez"];

class Musica{        /*se crea la clase musica**/
    nombre;
    genero;
    cantante;

    constructor(){
    }
    datos_canción(){  /*metodo para conocer los atributos del objeto definidos por un arreglo determinado*/
        let c;
        c=[(Math.floor(Math.random()*11))];
        let division=document.getElementById("Texto");
        division.innerHTML = "Esta sonando la canción: " + canciones[c] + " de: " + cantantes[c] + ", se ha repetido: " + c + " veces";
    }
}
class Juegos{       /*clase juegos*/
    constructor(name,numberG){
        this.name=name;
        this.numberG= numberG;
    }
    datosJ(){        /*metodo para conocer datos y estado del juego*/
        let division= document.getElementById("Texto");
        let c = [(Math.floor(Math.random()*5))];
        division.innerHTML =  "Este juego es: " + this.name + " para " + this.numberG + " jugadores"+ ", ha sido jugado " + c + " veces";
    }
}
class De_mesa extends Juegos{    /*subclase de juegos*/
    elementos_de_juego;
    constructor(name,numberG ,elments){
        super(name,numberG);
        this.elementos_de_juego=elments;
    }
}

class Videojuegos extends Juegos{     /*subclase de juegos*/
    categoria;
    constructor(name,numberG, category){
        super(name, numberG);
        this.categoria=category;
    }
}
class Comida{
    nombre;  /*Se crea la clase comida*/
    cantidad;
    sabor;
    constructor(name,quanty,taste){
        this.nombre=name;
        this.cantidad=quanty;
        this.sabor=taste;
    }
    datos(){    /*metodo para conocer datos de productos*/
        let division=document.getElementById("Texto");
        division.innerHTML=("Este producto es: " + this.nombre + "  de: " + this.cantidad + " sabe a: "+ this.sabor + " en este momento tiene: " + (Math.round(Math.random()*100)) + "% restante");
    }
}
class Bebida{
    nombre;  /*Se crea la clase bebida*/
    cantidad;
    porcentaje_alcohol;
    constructor(name,quanty,alcohol){
        this.nombre=name;
        this.cantidad=quanty;
        this.porcentaje_alcohol=alcohol;
    }
    datos(){    /*metodo para conocer datos de las bebidas*/
        let division=document.getElementById("Texto");
        division.innerHTML=("Esta bebida es: " + this.nombre + "  de:" + this.cantidad + " y tiene:" + this.porcentaje_alcohol + "% de alcohol");
    }
    restante(){    /*metodo para conocer restante de la bebida, se hace con un porcentaje aleatorio */
        let division=document.getElementById("Texto");
        let procentaje= Math.round(Math.random()*100);
        division.innerHTML= ( "El porcentaje restante de este producto es: " + procentaje);      
    }
}
class Invitado{  /*Se crea la clase invitado*/
    name; 
    age;
    constructor(name,age,drink,food,persona){
        this.name = name;
        this.age = age;
        this.drink=drink;
        this.food=food;
        this.persona=persona;
    }
    saludar(){    /*metodo para saludar*/
        let division=document.getElementById("Texto");
        division.innerHTML= ( "Hola mi nombre es " + this.name + " y tengo " + this.age+" años");
        }
    beber(){
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy bebiendo " + this.drink + " solo";  
        }
    beber_con(){
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy bebiendo " + this.drink + " con " + this.persona;     
    }
    
    comer(){
            let division=document.getElementById("Texto");
            division.innerHTML = "Solo vine para comer " + this.food + " con " + this.persona;    
        }
    }
class Jugador extends Invitado{     /*subclase de invitado*/
    constructor(name,age,game,loser){
        super(name,age);
        this.game=game;
        this.loser=loser;
    }
    
    reto_al_perdedor(){           /*metodo para dar reto a otro invitado l jugar*/
        let retos = new Array;
        retos=["hacer baile gracioso","Cantar muy alto pony salvaje", "dejarse maquillar", "besar a alguien en la fiesta", "pedir el número de alguien en la fiesta", "Hacer broma telefonica a alguno de sus contactos"];
        let c;
        c=[(Math.floor(Math.random()*6))];
        let division=document.getElementById("Texto");
        division.innerHTML = "He ganado en el juego " + this.game + " contra " + this.loser + " y ahora tendrá que: " + retos[c];
    }
    tirar_dados(){      /*metodo para tirar dados al jugar un juego de mesa que lo requiera*/
        let division=document.getElementById("Texto");
        division.innerHTML = " ";  
        let c;
        c=[(Math.floor(Math.random()*6))];
        division.innerHTML = "Estoy jugando "+ this.game+ " con "+ this.loser + ".He lanzado los dados y ha salido " + c;
    }
}

class Divertido extends Invitado{     /*subclase de invitad*/
    constructor(name,age,cancion,persona){
        super(name,age);
        this.cancion=cancion;
        this.persona=persona;
    }
    cantar(){   /*metodo para cantar*/
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy cantando " + this.cancion + " con " + this.persona ;  
    }
    bailar(){     /*metodo para bailar*/
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy bailando " + this.cancion + " con " + this.persona ;  
    }
    hacer_amistad(){    /*metodo para hacer amigos*/
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy hablando con " + this.persona + ", tenemos mucho en comun asi que mantendremos contacto luego de la fiesta"; 
    }
    gustar(){     /*metodo para gustar*/
        let division=document.getElementById("Texto");
        division.innerHTML = "Estoy hablando con " + this.persona + " me gusta y por ello le pediré salir luego de la fiesta";   
    }
}