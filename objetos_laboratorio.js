let comida1 = new Comida("Doritos", "58gr", "queso");
let comida2 = new Comida("Hamburguesa", "180 gr", "carne y pan");
let comida3 = new Comida("De todido", "150gr", "pollo");
let comida4 = new Comida("Salchipapas", "100gr", "papas y salchicha");
let comida5 = new Comida("Pizza", "1000gr", "masa y especias");
let comida6 = new Comida("Gomitas", "90gr", "saborizante de frutas");
let comida7 = new Comida("Bon bon bum", "19gr", "fresa");
let comida8 = new Comida("Papas risadas", "300gr", "mayonesa");
let comida9 = new Comida("Pollo asado", "1200gr", "ahumado");

let bebida1 = new Bebida("Gaseosa", "1200ml", 0);
let bebida2 = new Bebida("Aguardiente", "750 ml", 24);
let bebida3 = new Bebida("Smirnoff red", "700 ml", 37.5);
let bebida4 = new Bebida("Cerveza bbc Fourpack", "1076 ml", "3.9");
let bebida5 = new Bebida("Jugo", "1200 ml", 0);
let bebida6 = new Bebida("Ron", "375 ml", 35);

let song = new Musica();

let juego1 = new De_mesa("Uno","5","cartas");
let juego2 = new Videojuegos("crash","4","carreras");
let juego3 = new De_mesa("domino","6","fichas");
let juego4 = new De_mesa("parques", "4", "fichas y dados");

let invitado1 = new Invitado("Cristian", 18,  bebida4.nombre,comida9.nombre);
let invitado2 = new Jugador("Sandra", 20, juego1.name,invitado1.name);
let invitado3 = new Invitado ("Dayana", 19, bebida6.nombre, comida3.name,invitado2.name);
let invitado4 = new Divertido ("Wendy", 19, canciones[3],invitado3.name);
let invitado5 = new Divertido ("Felipe", 17, canciones[1],invitado4.name);
let invitado6 = new Divertido ("Andres", 18,canciones[1],invitado5.name);
let invitado7 = new Jugador ("Brayan", 19,juego4.name,invitado3.name);
let invitado8 = new Invitado ("Ana", 21,bebida6.nombre,comida6.nombre,invitado1.name);

